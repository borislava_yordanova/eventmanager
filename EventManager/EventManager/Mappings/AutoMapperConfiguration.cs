﻿using AutoMapper;
using EventManager.DAL;
using EventManager.ViewModels;

namespace EventManager.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Event, EventViewModel>();
                cfg.CreateMap<EventViewModel, Event>();
            });
        }
    }
}