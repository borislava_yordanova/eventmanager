﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventManager.Repositories;
using EventManager.DAL;
using AutoMapper;
using EventManager.ViewModels;

namespace EventManager.Controllers
{
    public class EventsController : Controller
    {
        private EventsEntities _db = new EventsEntities();
        private IRepository<Event> _repository = null;

        public EventsController()
        {
            this._repository = new Repository<Event>();
        }

        // GET: Events
        public ActionResult List()
        {
            var events = _repository.SelectAll().ToList();
            var eventViewModel = Mapper.Map<List<EventViewModel>>(events);
            return View(eventViewModel);
        }

        // GET: Events/Details/5
        public ActionResult Details(int? id)
        {
            var events = _repository.SelectByID(id.Value);
            var eventViewModel = Mapper.Map<EventViewModel>(events);
            return View(eventViewModel);
        }

        // GET: Events/Create
        public ActionResult Create()
        {
            var eventViewModel = new EventViewModel();
            return View(eventViewModel);
        }

        // POST: Events/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EventViewModel eventViewModel)
        {
            var events = new Event();
            var newEvent = Mapper.Map(eventViewModel, events);
            _repository.Insert(newEvent);
            _repository.Save();
            return RedirectToAction("List");
        }

        // GET: Events/Edit/5
        public ActionResult Edit(int? id)
        {
            var selectedEvent = _repository.SelectByID(id.Value);
            var eventViewModel = new EventViewModel();
            var editedEvent = Mapper.Map(selectedEvent, eventViewModel);
            return View(editedEvent);
        }

        // POST: Events/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id, EventViewModel eventViewModel)
        {
            var selectedEvent = _repository.SelectByID(id.Value);
            var editedEvent = Mapper.Map(eventViewModel, selectedEvent);
            _repository.Update(editedEvent);
            _repository.Save();
            return RedirectToAction("List");
        }

        // GET: Events/Delete/5
        public ActionResult Delete()
        {
            var model = new EventViewModel();
            return View(model);
        }

        // POST: Events/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(EventViewModel eventViewModel, int? id)
        {
            var selectedEvent = _repository.SelectByID(id.Value);
            var deletedEvent = Mapper.Map(eventViewModel, selectedEvent);
            _repository.Delete(deletedEvent);
            _repository.Save();
            return RedirectToAction("List");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
