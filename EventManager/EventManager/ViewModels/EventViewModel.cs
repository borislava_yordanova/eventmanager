﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventManager.ViewModels
{
    public class EventViewModel
    {
        [Key]
        public int Id { get; set; }

        [Display(Name="Event Name")]
        [Required(ErrorMessage = "Event Name field is reqiured")]
        [StringLength(50, MinimumLength = 8, ErrorMessage = "Field reqiures only letters and minimum 8 characters")]
        public string Name { get; set; }

        [Display(Name = "Event Location")]
        [Required(ErrorMessage = "Event Location field is reqiured")]
        [StringLength(100, MinimumLength = 10, ErrorMessage = "Field reqiures only letters and minimum 10 characters")]
        public string Location { get; set; }

        [Display(Name = "Start Date")]
        [Required(ErrorMessage = "Start Date field is reqiured")]
        public DateTime StartDate { get; set; }

        [Display(Name = "End Date")]
        [Required(ErrorMessage = "End Date field is reqiured")]
        public DateTime EndDate { get; set; }  
        
    }
}