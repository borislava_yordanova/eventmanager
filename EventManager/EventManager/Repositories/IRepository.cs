﻿using System.Collections.Generic;

namespace EventManager.Repositories
{
    public interface IRepository<T> 
    {
        IEnumerable<T> SelectAll();
        T SelectByID(int id);
        void Insert(T obj);
        void Update(T obj);
        void Delete(T obj);
        void Save();
    }
}