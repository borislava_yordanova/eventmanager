﻿using EventManager.Mappings;

namespace EventManager.App_Start
{
    public class Bootstraper
    {
        public static void Run()
        {
            AutoMapperConfiguration.Configure();
        }
    }
}