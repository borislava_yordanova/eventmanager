# README #

Event Manager is a web application that requires the following installations:
1.Install Visual Studio 2017 Community 2017
https://www.visualstudio.com/downloads/

2.Install SQL Server Management Studio (SSMS)
https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms

3.Go to Visual Studio 
Open the EventManager.sln 
Right click on View -> SQL Server Object Explorer 
Right Click on Databases  -> Add Database �Events�
Right Click on Tables -> Add new table


In the T-SQL section put this query

## CREATE TABLE [dbo].[Event] (
    [Id]        INT      PRIMARY KEY    IDENTITY (1, 1) NOT NULL,
    [Name]      NCHAR (50)    NOT NULL,
    [Location]  NCHAR (100)   NOT NULL,
    [StartDate] DATETIME2 (7) NOT NULL,
    [EndDate]   DATETIME2 (7) NOT NULL, );
##
    
-> Update database
	
4.Rebuild the project
Build -> Rebuild Solution

5.Run the project
